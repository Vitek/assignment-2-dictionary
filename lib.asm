section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy 
 
%define SYSEXIT 60
%define SYSPRINT 1
%define STDOUT 1
%define SYSREAD 0
%define STDIN 0
%define STREND 0
%define NEWLINE 0xA
%define ZEROCHAR 0x30
%define NINECHAR 0x39
%define TABCHAR 0x9
%define SPACECHAR 0x20
%define MINUSCHAR 0x2D
%define GOODRES 1
%define BADRES 0
%define CMPZEROVAL 0x8000000000000000 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSEXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 
 
    .loop: ; main loop starts here
        cmp byte [rdi+rax], STREND 
        je .end ; Jump if we found null-terminator
        inc rax ; Otherwise go to next symbol and increase counter
        jmp .loop

    .end:
        ret ; When we hit 'ret', rax should hold return value

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; указатель на строку в rdi
    call string_length ; нашли длину строки -> в rax
    mov rdx, rax ; сколько надо вывести бит
    mov rsi, rdi ; начало строки
    mov rax, SYSPRINT ; id write
    mov rdi, STDOUT ; stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSPRINT ; 'write' syscall identifier
    mov rdi, STDOUT ; stdout file descriptor
    mov rsi, rsp ; where do we take data from
    mov rdx, 1 ; the amount of bytes to write
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10
    xor rcx, rcx
    mov rax, rdi
    .loop:
        xor rdx, rdx
        div r8
        push rdx
        inc rcx
        cmp rax, STREND
        jnz .loop

    .rett:
        pop rdi
        add rdi, ZEROCHAR
        push rcx
        call print_char
        pop rcx
        dec rcx
        cmp rcx, STREND
        jnz .rett
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    mov r9, CMPZEROVAL
    test rax, r9
    jnz .under
    .above:
        call print_uint
        ret
    .under:
        mov rsi, rdi
        mov rdi, MINUSCHAR
        push rsi
        call print_char
        pop rsi
        mov rdi, rsi
        neg rdi
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    .loop:
        cmp byte[rdi+rax], STREND
        jz .chek
        cmp byte[rsi+rax], STREND
        jz .falsee
        mov r8b, [rdi+rax]
        mov r9b, [rsi+rax]
        cmp r8b, r9b
        jnz .falsee
        inc rax
        jmp .loop

    .chek:
        cmp byte[rsi+rax], STREND
        jnz .falsee
    .trueue:
        mov rax, GOODRES
        ret
    .falsee: 
        mov rax, BADRES
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rax, SYSREAD ; 'read' syscall identifier
    mov rdi, STDIN; stdin file descriptor
    mov rdx, 1 ; the amount of bytes to read
    push STREND ; null-terminator put in the stack
    mov rsi, rsp ; address where the string will be stored
    syscall
    pop rax ; load the symbol from the stack
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; rdi - началo буфера, rsi - размер буфера, rdx - длина строки
    xor rdx, rdx

    .firstloop:
        push rdi
        push rsi
        push rdx
        call read_char ; считали символ
        pop rdx
        pop rsi
        pop rdi

        cmp rax, SPACECHAR ; если пробел скип
        jz .firstloop
        cmp rax, TABCHAR
        jz .firstloop
        cmp rax, NEWLINE
        jz .firstloop

    .loop:

        cmp rax, STREND ; если конец ввода
        jz .goodend

        cmp rax, SPACECHAR ; если пробел
        jz .goodend

        cmp rsi, rdx ; если размер строки равен размеру буфера гг
        je .badend

        mov byte[rdi+rdx], al ; запись в буфер
        inc rdx ; длина +1

        push rdi
        push rsi
        push rdx
        call read_char ; считали символ
        pop rdx
        pop rsi
        pop rdi

        jmp .loop

    .goodend:
        mov byte[rdi+rdx], STREND
        mov rax, rdi
        ret
    .badend:
        xor rax, rax
        xor rdx, rdx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; rdi - начало строки, rdx - длина числа
    xor rdx, rdx
    xor rax, rax
    mov rcx, 10
    .loop:
        cmp byte[rdi+rdx], ZEROCHAR
        jl .end
        cmp byte[rdi+rdx], NINECHAR
        jg .end
        push rdx
        mul rcx
        pop rdx
        xor r8, r8
        add r8b, [rdi+rdx]
        add rax, r8
        sub rax, ZEROCHAR
        inc rdx
        jmp .loop 
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:

    cmp byte[rdi], MINUSCHAR ; если - 
    je .goodneg

    call parse_uint

    .end:
        ret 

    .goodneg:
        inc rdi
        call parse_uint
        cmp rdx, STREND
        jz .end
        inc rdx
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi - началo строки, rsi - начало буфера, rdx - длина буфера, rcx - длина строки
    xor rax, rax
    .loop:

        cmp rax, rdx
        je .bad

        cmp byte[rdi+rax], STREND
        je .end

        mov r8b, [rdi+rax]
        mov byte[rsi+rax], r8b

        inc rax
        jmp .loop

    .end:
        mov byte[rsi+rax], STREND ; нуль итератор
        ret

    .bad:
        xor rax, rax
        ret
