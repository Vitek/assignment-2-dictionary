%define m_begin 0

%macro colon 2 ; key / link name

	%2:
		dq m_begin ; old begin
		db %1, 0 ; key
	%define m_begin %2
                         ; value
%endmacro