global start

%include "lib.inc"
%include "words.inc"
extern find_word

%define buff_size 255
%define FD_STDERR 2
%define SYS_WRITE 1
%define ADDNUM 8
global _start

section .data
buff_overflow_mess:
db 'Buffer overflow',0

word_not_found_mess:
db 'Key not found',0

section .text
_start:
	sub rsp, buff_size
	mov rdi, rsp
	mov rsi, buff_size
	call read_word

	test rax, rax
	je .buff_overflow

	mov rdi, rax
	mov rsi, m_begin
	call find_word

	test rax, rax
	je .word_not_found


	add rax, ADDNUM
	mov rdi, rax
	push rax
    	call string_length
   	 pop rdi
    	add rax, rdi
    	inc rax
	mov rdi, rax
    	call print_string
    	call print_newline
    	call exit

	.buff_overflow:
		mov rdi, buff_overflow_mess
		jmp .print_err

	.word_not_found:
		mov rdi, word_not_found_mess
		jmp .print_err

	.print_err:
        push rdi
        call string_length
        pop rdi
        mov rdx, rax
        mov rsi, rdi
        mov rax, SYS_WRITE
        mov rdi, FD_STDERR
        syscall
        call print_newline
        call exit
