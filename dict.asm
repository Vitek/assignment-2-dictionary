section .text

extern string_equals

%define ADDNUM 8
%define CMPNUM 1

global find_word

find_word:
	.loop:

		test rsi, rsi
		je .bad

		push rdi
		push rsi
		add rsi, ADDNUM
		call string_equals
		pop rsi
		pop rdi

		cmp rax, CMPNUM
		je .good

		mov rsi, [rsi]
		jmp .loop

	.good:
		mov rax, rsi
		ret
	.bad:
		xor rax, rax
		ret

 
