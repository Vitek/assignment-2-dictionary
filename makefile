.PHONY: clean
clean:
        rm *.o
%.o: %.asm
        nasm -f elf64 -o $@ $<

main.o: main.asm dict.asm lib.asm lib.inc words.inc colon.inc
        nasm -f elf64 -o main.o main.asm

prog: main.o dict.o lib.o
        ld -o prog main.o dict.o lib.o
        make clean
